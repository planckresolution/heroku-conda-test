#!/usr/bin/env python

#Imports:
import os
import sys
import cv2
import numpy as np
import urllib
import pprint

                                                              
from flask import Flask
from flask import request                                                       
app = Flask(__name__)   

# Create the haar cascade
cascPath = "haarcascade_frontalface_default.xml"
faceCascade = cv2.CascadeClassifier(cascPath)

# Pretty Print objects:
pp = pprint.PrettyPrinter(indent=4)

def url_to_image(url):
    # download the image, convert it to a NumPy array, and then read it into OpenCV format
    resp = urllib.urlopen(url)
    image = np.asarray(bytearray(resp.read()), dtype="uint8")
    image = cv2.imdecode(image, cv2.IMREAD_COLOR)
    return image

@app.route('/')
def hello_world():
    url = request.args.get('url')
    image = url_to_image(url)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    faces = faceCascade.detectMultiScale(
        gray, scaleFactor=1.1, minNeighbors=5, minSize=(30, 30), flags = cv2.cv.CV_HAAR_SCALE_IMAGE)
    return "Found {0} faces!".format(len(faces)) + " \n" + pp.pformat(faces)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
